﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("SpeedFlight"))
        {
            Destroy(collision.gameObject);
            GameManager.Instance.DestroyedEnemy(5);
        }
        if (collision.CompareTag("Fragate"))
        {
            collision.gameObject.GetComponent<FragateController>().GetShot();
        }
    }
}
