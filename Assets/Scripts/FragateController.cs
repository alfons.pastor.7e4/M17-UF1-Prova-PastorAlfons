﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateController : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 0.05f;
    private Vector2 spriteBoundsSize;
    public float lifes = 5;
    // Start is called before the first frame update
    void Start()
    {
        spriteBoundsSize = gameObject.GetComponent<SpriteRenderer>().bounds.size;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        transform.position = transform.position + Vector3.down * movementSpeed;
        if (transform.position.y < -Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0)).y - spriteBoundsSize.y)
        {
            Destroy(gameObject);
        }
    }

    public void GetShot()
    {
        lifes = lifes - 1;
        if(lifes <= 0)
        {
            GameManager.Instance.DestroyedEnemy(20);
            Destroy(gameObject);
        }
    }
}
