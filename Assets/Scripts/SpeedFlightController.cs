﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightController : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 0.1f;
    private Vector2 spriteBoundsSize;

    // Start is called before the first frame update
    void Start()
    {
        spriteBoundsSize = gameObject.GetComponent<SpriteRenderer>().bounds.size;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        transform.position = transform.position + Vector3.down * movementSpeed;
        if (transform.position.y < -Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0)).y - spriteBoundsSize.y)
        {
            Destroy(gameObject);
        }
    }
}
