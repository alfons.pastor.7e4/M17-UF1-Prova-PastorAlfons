﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    [SerializeField]
    GameObject SpeedAlien;
    [SerializeField]
    GameObject FragateAlien;
    private Vector3 screenDimensions;
    private Vector2 spriteBoundsSizeSpeedAlien;
    private Vector2 spriteBoundsSizeFragateAlien;
    private int alienType;
    private System.Random rnd = new System.Random();
    // Start is called before the first frame update
    void Start()
    {
        screenDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        spriteBoundsSizeSpeedAlien = SpeedAlien.GetComponent<SpriteRenderer>().bounds.size;
        spriteBoundsSizeFragateAlien = FragateAlien.GetComponent<SpriteRenderer>().bounds.size;
        StartCoroutine(SpawnEnemies());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnEnemies()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.0f);
            alienType = Random.Range(0, 2);
            if(alienType == 0)
            {
                Instantiate(SpeedAlien, new Vector2(Random.Range(-screenDimensions.x + spriteBoundsSizeSpeedAlien.x, screenDimensions.x - spriteBoundsSizeSpeedAlien.x/2), transform.position.y), Quaternion.identity);
            }
            if (alienType == 1)
            {
                Instantiate(FragateAlien, new Vector2(Random.Range(-screenDimensions.x + spriteBoundsSizeFragateAlien.x, screenDimensions.x - spriteBoundsSizeFragateAlien.x / 2), transform.position.y), Quaternion.identity);
            }
        }
    }
}
