﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 0.5f;
    private Vector3 newX, newY, screenDimensions;
    private Vector2 spriteBoundsSize;
    [SerializeField]
    private GameObject bullet;
    private GameObject instantiatedBullet;
    // Start is called before the first frame update
    void Start()
    {
        screenDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        spriteBoundsSize = gameObject.GetComponent<SpriteRenderer>().bounds.size;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            instantiatedBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            instantiatedBullet.GetComponent<Rigidbody2D>().velocity = Vector3.up*20;
        }
    }
    private void FixedUpdate()
    {
        newX = transform.position + (new Vector3(Input.GetAxis("Horizontal"), 0) * movementSpeed);
        if(newX.x < screenDimensions.x - spriteBoundsSize.x/2 && newX.x > -screenDimensions.x + spriteBoundsSize.x/2)
        {
            transform.position = newX;
        }
        newY = transform.position + (new Vector3(0, Input.GetAxis("Vertical")) * movementSpeed);
        if (newY.y < screenDimensions.y - spriteBoundsSize.y/2 && newY.y > -screenDimensions.y + spriteBoundsSize.y/2)
        {
            transform.position = newY;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SceneManager.LoadScene("M17UF1-Prova");
    }
}
