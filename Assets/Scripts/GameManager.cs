﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public GameObject player;
    public int score = 0;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);

        }
        else
        {

            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    public void DestroyedEnemy(int points)
    {
        score = score + points;
    }
}
